from flask import Flask, jsonify

app = Flask(__name__)

# Dados fictícios dos resultados dos jogos da NBA
@app.route('/')
def home():
    return (
        '<h1>Seja bem-vindo Pag Inicial</h1>'
        '<p>Complemente acima no endereço para ver a API dos resultados <strong>/v1/resultados_nba</strong></p>'
        '<p>Caso queira usar o postman, utilize o link(URL) acima com <strong>/v1/resultados_nba</strong</p>'
        '<p>Exemplo: <strong>http://192.168.15.68:5000/v1/resultados_nba</strong></p>'
    )


@app.route('/v1/resultados_nba', methods=['GET'])
def get_resultados():
    resultados_nba = [
    {"data": "2024-04-26", "time_casa": "Los Angeles Lakers", "pontos_casa": 110, "time_visitante": "Golden State Warriors", "pontos_visitante": 105},
    {"data": "2024-04-25", "time_casa": "Brooklyn Nets", "pontos_casa": 115, "time_visitante": "Milwaukee Bucks", "pontos_visitante": 120},
    {"data": "2024-04-24", "time_casa": "Los Angeles Clippers", "pontos_casa": 98, "time_visitante": "Phoenix Suns", "pontos_visitante": 102}
]
    return jsonify(resultados_nba)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

